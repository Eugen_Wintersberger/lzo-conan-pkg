#include <iostream>
extern "C"{

#include <lzo/lzo1x.h>
}

int main() {
    if(lzo_init()!=LZO_E_OK)
    {
        return 1;
    }

    return 0;
}
