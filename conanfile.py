from conans import ConanFile, CMake, tools
import os


class LzoConan(ConanFile):
    name = "lzo"
    version = "2.10"
    license = "<Put the package license here>"
    url = "<Package recipe repository url here, for issues about the package>"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "*.tar.gz"

    def source(self):
        archive_file="lzo-2.10.tar.gz"
        tools.unzip(archive_file)

    def build(self):

        cmake = CMake(self)
        cmake_defs = {}
        cmake_defs["CMAKE_INSTALL_PREFIX"]=self.package_folder
        if self.options["shared"]:
            cmake_defs["ENABLE_SHARED"]="ON"
        
        cmake.configure(source_dir="lzo-2.10",
                        defs=cmake_defs)
        cmake.build()
        
        if self.settings.os=="Windows":
            cmake.build(target="RUN_TESTS")
        else:
            cmake.build(target="test")

        cmake.build(target="install")
        

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.libs = ["lzo"]
